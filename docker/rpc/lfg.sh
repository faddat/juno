export LATEST_HEIGHT=$(curl -s https://rpc-juno-ia.notional.ventures/block | jq -r .result.block.header.height);
export BLOCK_HEIGHT=$(($LATEST_HEIGHT-$INTERVAL)) 
export TRUST_HASH=$(curl -s "https://rpc-juno-ia.notional.ventures/block?height=$BLOCK_HEIGHT" | jq -r .result.block_id.hash)
export JUNOD_STATESYNC_TRUST_HEIGHT=$BLOCK_HEIGHT
export JUNOD_STATESYNC_TRUST_HASH=$TRUST_HASH
export PATH=$PATH:/go/bin
echo "TRUST HEIGHT: $BLOCK_HEIGHT"
echo "TRUST HASH: $TRUST_HASH"

junod init juno-matrix
cp /genesis.json ~/.junod/config/genesis.json
junod start --x-crisis-skip-assert-invariants --db_backend ${{ DATABASE }}