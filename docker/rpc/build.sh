#!/bin/bash
set -uxe

# We copy this into the docker container, and we use it to test the different databases
# it is poor form, but we also simply clone the code into the container. 

git clone https://github.com/CosmosContracts/juno
cd juno 
# this step replaces the database:
go mod edit -replace github.com/tendermint/tm-db=github.com/notional-labs/tm-db@10cdb28
go mod tidy

# force it to use static lib (from above) not standard libgo_cosmwasm.so file
LEDGER_ENABLED=false BUILD_TAGS=muslc LINK_STATICALLY=true go install -ldflags '-w -s -X github.com/cosmos/cosmos-sdk/types.DBBackend='$DATABASE  '-linkmode=external -extldflags "-Wl,-z,muldefs -static"' -tags $DATABASE ./...
file /go/bin/junod
echo "Ensuring binary is statically linked ..." \
  && (file /code/bin/junod | grep "statically linked")


